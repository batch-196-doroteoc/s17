/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessage(){
	let fullName = prompt('Enter your Full Name: ');
	let age = prompt('Enter your Age: ');
	let location = prompt('Enter your location: ')

	console.log('Hi, ' + fullName + "!");
	console.log(fullName +'\'s age is ' + age);
	console.log(fullName + ' is located at ' + location + '.');
};

printWelcomeMessage();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function favoriteBands(){
	let bands = ['BTS','Red Velvet', 'Journey', 'Yoasobi','Mamamoo'];
	console.log(bands);
};

favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function displayMovies(){

		let movie1 = 'Dead Poet\'s Society';
		let movie2 = 'Free Guy';
		let movie3 = 'Shutter Island';
		let movie4 = 'Pirates of the Caribbean';
		let movie5 = 'Eternal Sunshine of the Spotless Mind';
		let movieRating1 = '82%';
		let movieRating2 = '80%';
		let movieRating3 = '68%';
		let movieRating4 = '80%';
		let movieRating5 = '92%';

	console.log('1. ' + movie1);
	console.log('Tomato Meter for ' + movie1 + ':' + movieRating1);
	console.log('2. ' + movie2);
	console.log('Tomato Meter for ' + movie2 + ':' + movieRating2);
	console.log('3. ' + movie3);
	console.log('Tomato Meter for ' + movie3 + ':' + movieRating3);
	console.log('4. ' + movie4);
	console.log('Tomato Meter for ' + movie4 + ':' + movieRating4);
	console.log('5. ' + movie5);
	console.log('Tomato Meter for ' + movie5 + ':' + movieRating5);

};

displayMovies();

/*

	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



function printFriends(){

	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();




// yey