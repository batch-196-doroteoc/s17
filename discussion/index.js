//Functions are the heart of applications ---nagpapagalaw ng isang application, lines and block of codes, ano ang dapat gawin ng isang machine, para di paulit-ulit

/*
	Syntax:
		function functionName(){
			code block(statement)
		}
*/
// a functon should be invoked, tinatawag, function ilagay, pag di nainvooke hindi gagana

function printName() {
	console.log('My name is Anna');
};
// invocation
printName();

// declaredFunction();
//result: err, because it is not yet defined

//Function declaration vs. Expressions
// pag may ininvoke, dapat dineclare, naka reserve si declared function pwede magkapalit

declaredFunction ();

function declaredFunction(){
	console.log("Hi, I am from declaredFunction");
};

declaredFunction();

//you can invoke again and again, tatawagan pa rin nya si line 26

//Function Expression
	//anonymous function - function without a name - possible ang ga fnctuon na walang pangalan

// variableFunction(); hindi siya maghoist, naunang gamitin bago mainitialize


let variableFunction = function (){
	console.log("I am from variableFunction");
};

variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side");
};

funcExpression(); //siya kasi may hawak kay funcName, gamitin ang variable

//we can also reassign function

//You can reasssign declared functions and function expressions to new anonymous function

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();


//yung pnagaln ng declared expression value -- pwede maging variable name

funcExpression = function(){
	console.log('Updated funcExpression');
};

funcExpression();

const constantFunction = function(){
	console.log("Initialized with const");
};

constantFunction();

// constantFunction = function(){
// 	console.log("Cannot be reassigned!")
// };

// constantFunction();
//reassignment with const function experssion is not possible


//Function Scoping

/*
	Javascript Variables has 3 types of scope:
	1.local/block scope
	2. global scope
	3. function scope

*/

{
	let localVar = "Armando Perez";
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);
// console.log(localVar);


function showNames(){

	//function scoped variable0
	var functionVar = "Joe";
	const functionConst = "Nick";
	let functionLet = 'Kevin'


		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
};

showNames();
//result: err
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested Function

function myNewFunction(){
	let name = 'Yor';
	console.log(name);
	function nestedFunction(){
		let nestedName = 'Brando';
		console.log(nestedName);
	};
	//console.log (nestedName); result: err, nestedName for functions scope
	nestedFunction();
};

myNewFunction();

//Function and Globel Scoped Variable

//Global Scoped Variable

let globalName = "Alan";
function myNewFunction2() {
	let nameInside = "Marco";
	console.log(globalName);
}
myNewFunction2();
//console.log(nameInside); result: err

//alert()
/*
		Syntax:
		alert('message');


*/
// alert("Hello World");

function showSampleAlert(){
	alert('Hello User!');
}

// showSampleAlert();

console.log("I will only log in the console when the alert is dismissed.");

// prompt()

/*
	Syntax:
		prompt("<dialog>")
*/

// let samplePrompt = prompt('Enter your name:');
// console.log('Hello ' + samplePrompt);

// let sampleNullPrompt = prompt("Don't enter anything");
// console.log(sampleNullPrompt);
//if the prompt () is cancelled, the result will be null
//if there is not input in the prompt, the result will be empty string ' '


function printWelcomeMessage(){
	let firstName = prompt('Enter your first name: ');
	let lastName = prompt('Enter your last name: ');

	console.log('Hello, ' + firstName + " " + lastName + "!");
	console.log("Welcome to my page");
};

printWelcomeMessage();


//Function Naming Conventions

function getCourses(){
	let courses = ['Science 101','Arithmetic 103', 'Grammar 105'];
	console.log(courses);
};

getCourses();

//Avoid generic names to avoid confusion

function get(){
	let name= "Anya";
	console.log(name);
}

get();

function foo(){
	console.log(25%5);
}

foo();

//Name your functions in small caps, follow camelcase when naming functions

function displaycarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000")
}

displaycarInfo();




